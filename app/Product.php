<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Product extends Model
{
    //
    public function images(){
      return $this->hasMany(Image::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
