<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nuwave\Lighthouse\Support\Traits\RelayConnection;

class Profile extends Model
{
  use RelayConnection;
  protected $fillable = [
      'first_name', 'last_name', 'avatar'
  ];
}
