<?php

namespace App\Http\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLType;
use Nuwave\Lighthouse\Support\Interfaces\RelayType;

class ProfileType extends GraphQLType implements RelayType
{


    /**
     * Attributes of type.
     *
     * @var array
     */
    protected $attributes = [
        'name' => 'Profile',
        'description' => 'Profile of the user'
    ];


    /**
     * Get model by id.
     *
     * Note: When the root 'node' query is called, this method
     * will be used to resolve the type by providing the id.
     *
     * @param  mixed $id
     * @return mixed
     */
    public function resolveById($id)
    {
        return \App\Profile::find($id);
    }


    /**
     * Type fields.
     *
     * @return array
     */
    public function fields()
    {
        return [
          'first_name' => [
            'type' => Type::string(),
            'description' => 'The firts name of the user'
          ],
          'last_name' => [
            'type' => Type::string(),
            'description' => 'The last name of the user'
          ],
          'avatar' => [
            'type' => Type::string(),
            'description' => 'The avatar of the user'
          ],

       ];
   }
}
