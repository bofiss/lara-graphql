<?php

namespace App\Http\GraphQL\Types;

use GraphQL;
use App\Http\GraphQL\Connections\UserProfileConnection;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLType;
use Nuwave\Lighthouse\Support\Interfaces\RelayType;

class UserType extends GraphQLType implements RelayType
{


    /**
     * Attributes of type.
     *
     * @var array
     */
    protected $attributes = [
      'name' => 'User',
      'description' => 'A user',
    ];

    /**
     * Get model by id.
     *
     * Note: When the root 'node' query is called, this method
     * will be used to resolve the type by providing the id.
     *
     * @param  mixed $id
     * @return mixed
     */
    public function resolveById($id)
    {
        return \App\User::first($id);
    }

    /**
     * Type fields.
     *
     * @return array
     */
    public function fields()
    {
      return [
        'email' => [
          'type' => Type::string(),
          'description' => 'The email of the user'
        ],
        'name' => [
          'type' => Type::string(),
          'description' => 'The name of the user'
        ],

        'profile'  => GraphQL::connection(new UserProfileConnection)->field()

      ];

    }
}
