<?php

namespace App\Http\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Nuwave\Lighthouse\Support\Definition\GraphQLType;

class ImageType extends GraphQLType
{
    /**
     * Attributes of type.
     *
     * @var array
     */
    protected $attributes = [
        'name' => '',
        'description' => ''
    ];

    /**
     * Type fields.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
