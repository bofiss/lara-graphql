<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class Image extends Model
{
  protected $fillable = [
       'product_id', 'image'
   ];
}
