<?php
// routes/graphql.php

GraphQL::schema()->group(['namespace' => 'App\\Http\\GraphQL'], function () {
    GraphQL::schema()->type('user', 'Types\\UserType');
    GraphQL::schema()->type('profile', 'Types\\ProfileType');
   // ...
    GraphQL::schema()->query('viewer', 'Queries\\UserQuery');
});
